#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()

{   float arb_func(float x);
    void rf( float *x, float x0,float x1, float fx0, float fx1,int *itr);


    FILE *outp;
    outp=fopen("rf.txt ","w");
    int itr;
    int maxitr;
    float x0;
    float x1;
    float x_curr;
    float x_next;
    float error;
    printf("enter intervsal values  [x0, x1 ], allowed error and number of iterations ");
    scanf("%f %f %f %d",&x0,&x1,&error,&maxitr);
    rf(&x_curr,x0,x1,arb_func(x0),arb_func(x1),&itr);
    do{

    if(x0*x_curr<0){
        x1=x_curr;

    }
    else{

        x0=x_curr;

    }

    rf(&x_next,x0,x1,arb_func(x0),arb_func(x1),&itr);
    if(fabs(x_next-x_curr < error)){
    printf("after %d iterations, root is %f  \n",maxitr ,arb_func(x_curr));
    return 0;

   }
   else{

    x_next=x_curr;
   }

    }while (itr< maxitr);

    printf("Solution does not converge or iterations not sufficient");



    fclose(outp);

    return 1;
}


float arb_func(float x){
    x=x*(log10(x))-1.2;

    return x;
}

void rf( float *x, float x0,float x1,float fx0, float fx1,int *itr){
    *x=((x0*fx1)-(x1*fx0))/(fx1-fx0);
    ++*itr;
    printf ("iteration %d : %.5f \n ", *itr, *x);
    FILE *outp;
    outp=fopen("rf.txt ","w");
    fprintf(outp," iteration  %.3d :  %.5f ", *itr, *x);

}

